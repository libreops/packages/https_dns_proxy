# https_dns_proxy

A lightweight DNS-over-HTTPS proxy

## download

binary from here:

- [https_dns_proxy](https://gitlab.com/libreops/packages/https_dns_proxy/-/jobs/artifacts/master/raw/https_dns_proxy?job=run-build)

## dependencies

```bash
pacman -S c-ares libev

```

## run

foreground

```bash
./https_dns_proxy -a 127.0.0.1 -p 5454 -v -b 116.202.176.26 -r https://doh.libredns.gr/ads

```

## testing

```bash
$ dig @127.0.0.1 -p 5454 google.com

; <<>> DiG 9.16.3 <<>> @127.0.0.1 -p 5454 google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 522
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		300	IN	A	172.217.16.142

;; Query time: 560 msec
;; SERVER: 127.0.0.1#5454(127.0.0.1)
;; WHEN: Sun May 24 21:41:09 EEST 2020
;; MSG SIZE  rcvd: 55
```

```bash
$ dig @127.0.0.1 -p 5454 analytics.google.com

; <<>> DiG 9.16.3 <<>> @127.0.0.1 -p 5454 analytics.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;analytics.google.com.		IN	A

;; ANSWER SECTION:
analytics.google.com.	74703	IN	A	0.0.0.0

;; Query time: 140 msec
;; SERVER: 127.0.0.1#5454(127.0.0.1)
;; WHEN: Sun May 24 21:41:16 EEST 2020
;; MSG SIZE  rcvd: 65

```

```bash
$ dig @127.0.0.1 -p 5454 test.libredns.gr

; <<>> DiG 9.16.3 <<>> @127.0.0.1 -p 5454 test.libredns.gr
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57598
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;test.libredns.gr.		IN	A

;; ANSWER SECTION:
test.libredns.gr.	3600	IN	A	116.202.176.26

;; AUTHORITY SECTION:
libredns.gr.		1008	IN	SOA	ns1.gandi.net. hostmaster.gandi.net. 1589895435 10800 3600 604800 10800

;; Query time: 150 msec
;; SERVER: 127.0.0.1#5454(127.0.0.1)
;; WHEN: Sun May 24 21:41:20 EEST 2020
;; MSG SIZE  rcvd: 121

```
